package com.thpa.a9019.androidmvmlogin.Interface;

public interface LoginResultCallbacks {
    void onSuccess(String message);
    void onError(String message);

}
