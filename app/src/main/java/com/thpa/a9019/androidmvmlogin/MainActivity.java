package com.thpa.a9019.androidmvmlogin;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.thpa.a9019.androidmvmlogin.Interface.LoginResultCallbacks;
import com.thpa.a9019.androidmvmlogin.ViewModel.LoginViewModel;
import com.thpa.a9019.androidmvmlogin.ViewModel.LoginViewModelFactory;
import com.thpa.a9019.androidmvmlogin.databinding.ActivityMainBinding;

import es.dmoral.toasty.Toasty;

public class MainActivity extends AppCompatActivity implements LoginResultCallbacks {
    private ActivityMainBinding activityMainBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        activityMainBinding.setViewModel(ViewModelProviders.of(this, new LoginViewModelFactory(this))
                .get(LoginViewModel.class));
    }

    @Override
    public void onSuccess(String message) {
        Toasty.success(this,message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String message) {
        Toasty.success(this,message, Toast.LENGTH_SHORT).show();

    }
}
