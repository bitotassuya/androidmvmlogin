package com.thpa.a9019.androidmvmlogin.ViewModel;

import android.arch.lifecycle.ViewModel;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.thpa.a9019.androidmvmlogin.Interface.LoginResultCallbacks;
import com.thpa.a9019.androidmvmlogin.Model.User;

public class LoginViewModel extends ViewModel {
    private User user;
    private LoginResultCallbacks loginResultCallbacks;
    private int errorCode;

    public LoginViewModel(LoginResultCallbacks loginResultCallbacks) {
        this.loginResultCallbacks = loginResultCallbacks;
        this.user = new User();
    }

    public TextWatcher getEmailTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                user.setEmail(s.toString());
            }
        };
    }

    public TextWatcher getPasswordTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                user.setPassword(s.toString());
            }
        };
    }

    //write method to process login
    public void onLoginClicked(View view) {
        errorCode = user.isValidData();

        if (errorCode == 0)
            loginResultCallbacks.onError("Uou must enter email address");
        else if (errorCode == 1)
            loginResultCallbacks.onError("Your Email invalid!");
        else if (errorCode == 2)
            loginResultCallbacks.onError("Your password mush have last than 6 char");
        else
            loginResultCallbacks.onSuccess("Login Success");
    }


}
